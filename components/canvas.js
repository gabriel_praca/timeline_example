/*React Native TimeLine ListView / Flatlist*/
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, ScrollView, FlatList } from 'react-native';
import Canvas from 'react-native-canvas';
 
export default class Test extends Component {
    constructor() {
        super();
    }
    handleCanvas = (canvas) => {
        const ctx = canvas.getContext('2d');
        ctx.fillStyle = 'purple';
        ctx.fillRect(0, 0, 100, 100);
    }
	render() {
        let dungeon = this.createMap();
		return (
            <Canvas ref={this.handleCanvas}/>
		);
	}
}