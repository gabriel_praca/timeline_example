/*React Native TimeLine ListView / Flatlist*/
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, SectionList, FlatList } from 'react-native';
// import Timeline from 'react-native-timeline-flatlist';
import Timeline from 'react-native-timeline-feed';
 
function Item({ item, length }) {
	return (
	  <View style={styles.item}>
		  {console.log(item)}
		<Text style={styles.title}>{item.time}</Text>
		<Text style={styles.title}>{item.description}</Text>
		<Text style={styles.title}>aaa: {length}</Text>
	  </View>
	);
  }
export default class OverrideRenderTimeLine extends Component {
  constructor() {
    super();
    this.onEventPress = this.onEventPress.bind(this);
    this.renderSelected = this.renderSelected.bind(this);
	this.renderDetail = this.renderDetail.bind(this);
	
    this.data = [
		{

			title: 'Event 1',
			data: [
				{
					time: '09:00',
					description: '1',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
				{
					time: '10:00',
					description: '2',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
				{
					time: '11:00',
					description: '3',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
				{
					time: '12:00',
					description: '4',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
			]
		},
		{

			title: 'Event 2',
			data: [
				{
					time: '11:00',
					description: '5',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
				{
					time: '12:00',
					description: '6',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
				{
					time: '10:00',
					description: '7',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
				{
					time: '10:00',
					description: '8',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
			]
		},
		{

			title: 'Event 3',
			data: [
				{
					time: '13:00',
					description: '9',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
				{
					time: '14:00',
					description: '10',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
				{
					time: '10:00',
					description: '11',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
				{
					time: '10:00',
					description: '12',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
				{
					time: '10:00',
					description: '12',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
				{
					time: '10:00',
					description: '12',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
				{
					time: '10:00',
					description: '12',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
				{
					time: '10:00',
					description: '12',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
			]
		},
		{

			title: 'Event 4',
			data: [
				{
					time: '13:00',
					description: '13',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
				{
					time: '14:00',
					description: '14',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
				{
					time: '10:00',
					description: '15',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
				{
					time: '10:00',
					description: '16',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
				{
					time: '10:00',
					description: '12',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
				{
					time: '10:00',
					description: '12',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
				{
					time: '10:00',
					description: '12',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
			]
		},
		{

			title: 'Event 5',
			data: [
				{
					time: '13:00',
					description: '17',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
				{
					time: '14:00',
					description: '18',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
				{
					time: '10:00',
					description: '19',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
				{
					time: '10:00',
					description: '20',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
				{
					time: '10:00',
					description: '12',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
				{
					time: '10:00',
					description: '12',					
					lineColor: '#009688',
					icon: require('../img/place_holder.png'),
					imageUrl:
					  'https://images.pexels.com/photos/2250394/pexels-photo-2250394.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=250&w=250',
				},
			]
		},
	];
    this.state = { selected: null };
  }
 
  onEventPress(data) {
    this.setState({ selected: data });
  }
 
  renderSelected() {
    if (this.state.selected)
      return (
        <Text style={{ marginTop: 10 }}>
          Selected event: {this.state.selected.title} at{' '}
          {this.state.selected.time}
        </Text>
      );
  }
 
  renderDetail(rowData, sectionID, rowID) {
    let title = <Text style={[styles.title]}>{rowData.title}</Text>;
    var desc = null;
    if (rowData.description && rowData.imageUrl)
      desc = (
        <View style={styles.descriptionContainer}>
          <Image source={{ uri: rowData.imageUrl }} style={styles.image} />
          <Text style={[styles.textDescription]}>{rowData.description}</Text>
        </View>
      );
 
    return (
      <View style={{ flex: 1 }}>
        {title}
        {desc}
      </View>
    );
  }
 
	render() {
		return (
			<View style={styles.container}>
				<View style={
					{
						marginHorizontal: 15, 
						flexDirection: 'row', 
						marginVertical: 10,
						// borderColor: 'red', 
						// borderWidth: 1, 
						paddingVertical: 15,
						borderRadius: 5,
						shadowColor: "#000",
						shadowOffset: {
							width: 0,
							height: 1,
						},
						shadowOpacity: 0.22,
						shadowRadius: 2.22,
						
						elevation: 3,
					}}>
					<View style={{marginVertical: 10, marginHorizontal: 25, alignItems: 'center', justifyContent: 'center'}}>
						<Text style={{backgroundColor: '#ccc', borderRadius: 25, width: 50, height: 50, textAlign: 'center', textAlignVertical: 'center'}}>Foto</Text>
						<Text style={{fontSize: 12}}>Fulano</Text>
					</View>
					<View style={{
							flex:1, 
							justifyContent: 'center', 
							// borderColor: '', 
							// borderWidth: 1
							}}>
						<Text style={{alignSelf: 'center'}}>-1 de vida</Text>
						{/* <Text style={{alignSelf: 'center'}}>Matou um goblin</Text> */}

						<View style={{marginVertical: 15}}>
							{/* <Image source={{ uri: 'https://i.pinimg.com/originals/ae/7c/01/ae7c0199c0103b3a6a378f151644bc72.png' }} style={styles.image} /> */}
						</View>
						<View style={{alignItems: 'flex-end', paddingRight: 15}}>
							<Text style={{fontSize: 9}}>01/01/1111 ás 11:11</Text>
						</View>
					</View>
				</View>
				<View style={
					{
						marginHorizontal: 15, 
						flexDirection: 'row', 
						marginVertical: 10,
						// borderColor: 'red', 
						// borderWidth: 1, 
						paddingVertical: 15,
						borderRadius: 5,
						shadowColor: "#000",
						shadowOffset: {
							width: 0,
							height: 1,
						},
						shadowOpacity: 0.22,
						shadowRadius: 2.22,
						
						elevation: 3,
					}}>
					<View style={{ marginHorizontal: 25, alignItems: 'center', justifyContent: 'center'}}>
						<Text style={{backgroundColor: '#ccc', borderRadius: 25, width: 50, height: 50, textAlign: 'center', textAlignVertical: 'center'}}>Foto</Text>
						<Text style={{fontSize: 12}}>Fulano</Text>
					</View>
					<View style={{
							flex:1, 
							justifyContent: 'center', 
							// borderColor: '', 
							// borderWidth: 1
							}}>
						<Text style={{alignSelf: 'center'}}>Acessou uma dungeon</Text>

						<View style={{marginVertical: 15}}>
							<Image source={{ uri: 'https://vignette.wikia.nocookie.net/khanpower/images/2/26/Boggey_Dungeon.gif/revision/latest?cb=20110828092519' }} style={styles.image} />
						</View>
						<View style={{alignItems: 'flex-end', paddingRight: 15}}>
							<Text style={{fontSize: 9}}>01/01/1111 ás 11:11</Text>
						</View>
					</View>
				</View>
				{/* <FlatList
					data={this.data}
					renderItem={({ item }) => 
					<View>
						<Text>{item.title}</Text>
						<FlatList
							data={item.data}
							renderItem={( item ) => 
								
								{console.log(item)}
							}
							keyExtractor={(item, index) => item + index}
						/>
					</View>
					}
					keyExtractor={(item, index) => item + index}
				/> */}
				{/* <SectionList
					ref={"SectionList"}
					stickySectionHeadersEnabled={true}
					sections={this.data}
					keyExtractor={(item, index) => item + index}
					renderItem={( item ) => 
						<Item item={item} length={[item].length}/>
					}
					renderSectionHeader={({ section: { title } }) => (
						<Text style={{fontSize: 24, backgroundColor: 'white'}}>{title}</Text>
						// <Text
						// 	style={{
						// 	padding: 16,
						// 	fontSize: 20,
						// 	textAlign: 'center',
						// 	fontWeight: 'bold',
						// 	}}>
						// 	Override Render TimeLine Example
						// </Text>,
						// this.renderSelected(),
						// <Timeline
						// 	style={styles.list}
						// 	data={this.data}
						// 	circleSize={20}
						// 	circleColor="rgba(0,0,0,0)"
						// 	lineColor="rgb(45,156,219)"
						// 	timeContainerStyle={{ minWidth: 52, marginTop: -5 }}
						// 	timeStyle={{
						// 	textAlign: 'center',
						// 	backgroundColor: '#ff9797',
						// 	color: 'white',
						// 	padding: 5,
						// 	borderRadius: 13,
						// 	}}
						// 	descriptionStyle={{ color: 'gray' }}
						// 	options={{
						// 	style: { paddingTop: 5 },
						// 	}}
						// 	innerCircle={'icon'}
						// 	onEventPress={this.onEventPress}
						// 	renderDetail={this.renderDetail}
						// />
					)}
				/> */}
			</View>
		);
	}
}
 
const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // padding: 20,
    // backgroundColor: 'white',
    // marginTop: 50,
    // marginHorizontal: 16,
  },
  list: {
    flex: 1,
    marginTop: 20,
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  descriptionContainer: {
    flexDirection: 'row',
    paddingRight: 50,
  },
  image: {
    width: 150,
	height: 150,
	alignSelf: 'center',
    borderRadius: 25,
  },
  textDescription: {
    marginLeft: 10,
    color: 'gray',
  },
});