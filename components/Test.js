/*React Native TimeLine ListView / Flatlist*/
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, ScrollView, FlatList } from 'react-native';
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component';
 
export default class Test extends Component {
  constructor() {
    super();
  }
    createArray(num, dimensions) {
        var array = [];
        for (var i = 0; i < dimensions; i++) {
            array.push([]);
            for (var j = 0; j < dimensions; j++) {
                array[i].push(num);
            }
        }
        return array;
    }    
    //lets create a randomly generated map for our dungeon crawler
    createMap() {
        let dimensions = 50;//this.state.dimensions, // width and height of the map
        let maxTunnels = dimensions*2;//this.state.maxTunnels, // max number of tunnels possible
        let maxLength = 5;//this.state.maxLength, // max length each tunnel can have
        let map = this.createArray(1, dimensions); // create a 2d array full of 1's
        let currentRow = Math.floor(Math.random() * dimensions); // our current row - start at a random spot
        let currentColumn = Math.floor(Math.random() * dimensions); // our current column - start at a random spot
        let directions = [[-1, 0], [1, 0], [0, -1], [0, 1]]; // array to get a random direction from (left,right,up,down)
        let lastDirection = []; // save the last direction we went
        let randomDirection; // next turn/direction - holds a value from directions
        
        // lets create some tunnels - while maxTunnels, dimentions, and maxLength  is greater than 0.
        while (maxTunnels && dimensions && maxLength) {
            
            // lets get a random direction - until it is a perpendicular to our lastDirection
            // if the last direction = left or right,
            // then our new direction has to be up or down,
            // and vice versa
            do {
                randomDirection = directions[Math.floor(Math.random() * directions.length)];
            } 
            while ((randomDirection[0] === -lastDirection[0] && randomDirection[1] === -lastDirection[1]) || (randomDirection[0] === lastDirection[0] && randomDirection[1] === lastDirection[1]));
        
            var randomLength = Math.ceil(Math.random() * maxLength), //length the next tunnel will be (max of maxLength)
            tunnelLength = 0; //current length of tunnel being created
        
            // lets loop until our tunnel is long enough or until we hit an edge
            while (tunnelLength < randomLength) {
        
                //break the loop if it is going out of the map
                if (((currentRow === 0) && (randomDirection[0] === -1)) ||
                    ((currentColumn === 0) && (randomDirection[1] === -1)) ||
                    ((currentRow === dimensions - 1) && (randomDirection[0] === 1)) ||
                    ((currentColumn === dimensions - 1) && (randomDirection[1] === 1))) {
                    break;
                } else {
                    map[currentRow][currentColumn] = 0; //set the value of the index in map to 0 (a tunnel, making it one longer)
                    currentRow += randomDirection[0]; //add the value from randomDirection to row and col (-1, 0, or 1) to update our location
                    currentColumn += randomDirection[1];
                    tunnelLength++; //the tunnel is now one longer, so lets increment that variable
                }
            }
        
            if (tunnelLength) { // update our variables unless our last loop broke before we made any part of a tunnel
                lastDirection = randomDirection; //set lastDirection, so we can remember what way we went
                maxTunnels--; // we created a whole tunnel so lets decrement how many we have left to create
            }
        }
        return map; 
    }

    renderMap() {
        let dungeon = this.createMap();
        

        <View style={styles.container}>
            <Table borderStyle={{borderColor: 'transparent'}}>
                {/* <Row data={state.tableHead} style={styles.head} textStyle={styles.text}/> */}
                {
                dungeon.map((rowData, index) => (
                    <TableWrapper key={index} style={styles.row}>
                    {
                        rowData.map((cellData, cellIndex) => (
                        <Cell key={cellIndex} data={cellData} textStyle={styles.text}/>
                        ))
                    }
                    </TableWrapper>
                ))
                }
            </Table>
        </View>
    }
	render() {
        let dungeon = this.createMap();
		return (
            
            <ScrollView vertical style={{ flex: 1, backgroundColor: 'pink'}}>
                <ScrollView horizontal style={{backgroundColor: 'blue'}}>
                    <View style={styles.container}>
                        {
                            dungeon.map(line => (
                                <View style={{flexDirection: 'row', padding: 0, margin: 0}}>
                                    {
                                        line.map(cel => (
                                            <View style={{padding: 0, margin: 0}}>
                                                {
                                                    cel === 0 ? 
                                                    <Text style={{backgroundColor: '#8F6B44', height: 10, width: 10}}/>
                                                    :
                                                    <Text style={{backgroundColor: '#000', height: 10, width: 10}}/>
                                                }
                                            </View>
                                        ))
                                    }
                                </View>
                            ))
                        }
                    </View>
                </ScrollView>
            </ScrollView>
		);
	}
}
 
const styles = StyleSheet.create({
  
    container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
    head: { height: 40, backgroundColor: '#808B97' },
    text: { margin: 6 },
    row: { flexDirection: 'row', backgroundColor: '#FFF1C1' },
    btn: { width: 58, height: 18, backgroundColor: '#78B7BB',  borderRadius: 2 },
    btnText: { textAlign: 'center', color: '#fff' },
  list: {
    flex: 1,
    marginTop: 20,
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  descriptionContainer: {
    flexDirection: 'row',
    paddingRight: 50,
  },
  image: {
    width: 150,
	height: 150,
	alignSelf: 'center',
    borderRadius: 25,
  },
  textDescription: {
    marginLeft: 10,
    color: 'gray',
  },
});